import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Item } from 'src/app/models/item';

@Component({
  selector: 'app-full-item-details',
  templateUrl: './full-item-details.component.html',
  styleUrls: ['./full-item-details.component.scss'],
})
export class FullItemDetailsComponent implements OnInit {
  constructor(@Inject(MAT_DIALOG_DATA) public data: Item) {}

  ngOnInit(): void {}
}
