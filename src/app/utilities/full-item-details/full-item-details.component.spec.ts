import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FullItemDetailsComponent } from './full-item-details.component';

describe('FullItemDetailsComponent', () => {
  let component: FullItemDetailsComponent;
  let fixture: ComponentFixture<FullItemDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FullItemDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FullItemDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
