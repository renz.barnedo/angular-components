import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Item } from 'src/app/models/item';
import { DataService } from 'src/app/services/data.service';
import { UtilitiesService } from 'src/app/services/utilities.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  itemForm: FormGroup = this.fb.group({});

  displayedColumns: string[] = [];
  dataSource: any;

  @ViewChild(MatPaginator) paginator: any;
  @ViewChild(MatSort) sort: any;

  constructor(
    private dataService: DataService,
    private fb: FormBuilder,
    private utilitiesService: UtilitiesService
  ) {}

  async ngOnInit() {
    try {
      this.hideUi();
      this.setItemForm();
      await this.setTable();
      this.displayUi();
    } catch (error) {
      this.utilitiesService.openDialog('Error', 'Cannot set UI. Try again.');
    }
  }

  async setTable() {
    const response: any = await this.dataService.getItems();
    this.displayedColumns = ['select', 'name', 'price', 'update'];
    this.dataSource = new MatTableDataSource(response);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  setItemForm(item: any = null) {
    let name: string = '';
    let price: string = '';
    let description: string = '';
    let imageUrl: string = '';
    let _id: string = '';

    if (item) {
      name = item.name;
      price = item.price;
      description = item.description;
      imageUrl = item.imageUrl;
      _id = item._id;
    }

    const controls: any = {
      name: this.fb.control(name, [
        Validators.required,
        Validators.maxLength(20),
      ]),
      price: this.fb.control(price, [
        Validators.required,
        Validators.max(1000),
      ]),
      description: this.fb.control(description, [
        Validators.required,
        Validators.maxLength(100),
      ]),
      imageUrl: this.fb.control(imageUrl, Validators.required),
    };

    if (_id) {
      controls._id = this.fb.control(_id, Validators.required);
    }

    this.itemForm = this.fb.group(controls);
  }

  async submitItemForm() {
    if (this.itemForm.status !== 'VALID') {
      this.utilitiesService.openDialog('Error', 'Please fix the fields.');
      return;
    }
    this.utilitiesService.openLoadingSpinner();
    const item: Item = this.itemForm.value;

    if (!item._id) {
      await this.addItem(item);
      return;
    }

    const id = item._id;
    delete item._id;
    this.updateItem(item, id);
  }

  async addItem(item: Item) {
    try {
      await this.dataService.addItem(item);
      await this.setTable();
      this.itemForm.reset();
      this.utilitiesService.closeLoadingSpinner();
      this.utilitiesService.openSnackBar(`Added item: ${item.name}`, 'Success');
    } catch (error) {
      this.utilitiesService.openDialog('Error', 'Cannot add item. Try again.');
    }
  }

  setUpdateItem(item: Item) {
    this.setItemForm(item);
  }

  cancelUpdate() {
    this.itemForm.reset();
    this.setItemForm();
  }

  async updateItem(item: Item, id: string) {
    try {
      await this.dataService.updateItem(item, id);
      await this.setTable();
      this.utilitiesService.closeLoadingSpinner();
      this.utilitiesService.openSnackBar(`Updated: ${item.name}`, 'Success');
    } catch (error) {
      this.utilitiesService.openDialog('Error', "Can't update, try again.");
    }
  }

  async deleteItems() {
    try {
      this.utilitiesService.openLoadingSpinner();
      let selectedItems: any[] = this.selection.selected;
      selectedItems = selectedItems.map(async (item: any) => {
        await this.deleteItem(item._id);
      });
      await Promise.all(selectedItems);
      await this.setTable();
      this.selection.clear();
      this.utilitiesService.closeLoadingSpinner();
      this.utilitiesService.openSnackBar('Deleted item(s)', 'Success');
    } catch (error) {
      this.utilitiesService.openDialog('Error', "Can't delete, try again.");
    }
  }

  async deleteItem(id: string) {
    try {
      await this.dataService.deleteItem(id);
    } catch (error) {
      this.utilitiesService.openDialog('Error', "Can't delete, try again.");
    }
  }

  viewFullItemDetails(item: Item) {
    this.utilitiesService.openCardDialog(item);
  }

  selection = new SelectionModel<Item>(true, []);

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach((row: Item) => this.selection.select(row));
  }

  showUi: boolean = false;

  hideUi() {
    this.showUi = false;
    this.utilitiesService.openLoadingSpinner();
  }

  displayUi() {
    this.showUi = true;
    this.utilitiesService.closeLoadingSpinner();
  }
}
