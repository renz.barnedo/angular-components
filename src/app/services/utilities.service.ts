import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Item } from '../models/item';
import { FullItemDetailsComponent } from '../utilities/full-item-details/full-item-details.component';
import { NotificationDialogComponent } from '../utilities/notification-dialog/notification-dialog.component';
import { SpinnerComponent } from '../utilities/spinner/spinner.component';

@Injectable({
  providedIn: 'root',
})
export class UtilitiesService {
  constructor(private dialog: MatDialog, private snackBar: MatSnackBar) {}

  openLoadingSpinner() {
    this.dialog.open(SpinnerComponent, {
      disableClose: true,
      panelClass: 'transparent',
    });
  }

  closeLoadingSpinner() {
    this.dialog.closeAll();
  }

  openCardDialog(item: Item) {
    this.dialog.open(FullItemDetailsComponent, {
      data: item,
    });
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      verticalPosition: 'top',
      duration: 2000,
    });
  }

  openDialog(title: string, message: string) {
    this.dialog.open(NotificationDialogComponent, { data: { title, message } });
  }
}
