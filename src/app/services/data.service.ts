import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { Item } from '../models/item';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  API_URL: string = 'https://angular-2ea8.restdb.io/rest/inventory';
  OPTIONS: any = {
    headers: {
      'x-api-key': environment.apiKey,
    },
  };

  constructor(private http: HttpClient) {}

  addItem(item: Item) {
    return new Promise((resolve, reject) =>
      this.http.post(this.API_URL, item, this.OPTIONS).subscribe(
        (response: any) => resolve(response),
        (error: any) => reject(error)
      )
    );
  }

  getItems() {
    return new Promise((resolve, reject) =>
      this.http.get(this.API_URL, this.OPTIONS).subscribe(
        (response: any) => resolve(response),
        (error: any) => reject(error)
      )
    );
  }

  updateItem(item: Item, _id: string) {
    return new Promise((resolve, reject) =>
      this.http.put(`${this.API_URL}/${_id}`, item, this.OPTIONS).subscribe(
        (response: any) => resolve(response),
        (error: any) => reject(error)
      )
    );
  }

  deleteItem(_id: string) {
    return new Promise((resolve, reject) =>
      this.http.delete(`${this.API_URL}/${_id}`, this.OPTIONS).subscribe(
        (response: any) => resolve(response),
        (error: any) => reject(error)
      )
    );
  }
}
